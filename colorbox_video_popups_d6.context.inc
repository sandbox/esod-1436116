<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function colorbox_video_popups_d6_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = '';
  $context->tag = 'sitewide';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'user-1' => array(
          'module' => 'user',
          'delta' => 1,
          'region' => 'left',
          'weight' => 0,
        ),
        'user-0' => array(
          'module' => 'user',
          'delta' => 0,
          'region' => 'left',
          'weight' => 1,
        ),
        'views-video-block_1' => array(
          'module' => 'views',
          'delta' => 'video-block_1',
          'region' => 'content',
          'weight' => 0,
        ),
        'system-0' => array(
          'module' => 'system',
          'delta' => 0,
          'region' => 'footer',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sitewide');
  $export['sitewide'] = $context;

  return $export;
}
