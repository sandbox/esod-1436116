<?php

/**
 * Implementation of hook_content_default_fields().
 */
function colorbox_video_popups_d6_content_default_fields() {
  $fields = array();

  // Exported field: field_video
  $fields['video-field_video'] = array(
    'field_name' => 'field_video',
    'type_name' => 'video',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'emvideo',
    'required' => '0',
    'multiple' => '0',
    'module' => 'emvideo',
    'active' => '1',
    'widget' => array(
      'video_width' => '636',
      'video_height' => '380',
      'video_autoplay' => 1,
      'preview_width' => '640',
      'preview_height' => '390',
      'preview_autoplay' => 0,
      'thumbnail_width' => '120',
      'thumbnail_height' => '90',
      'thumbnail_default_path' => '',
      'thumbnail_link_title' => 'See video',
      'meta_fields' => array(
        'title' => 0,
        'description' => 0,
      ),
      'providers' => array(
        'vimeo' => 'vimeo',
        'youtube' => 'youtube',
      ),
      'emthumb' => 1,
      'emthumb_label' => 'Video custom thumbnail',
      'emthumb_description' => 'If you upload a custom thumbnail, then this will be displayed when the Video thumbnail is called for, overriding any automatic thumbnails by custom providers.',
      'emthumb_max_resolution' => '1280x1280',
      'emimport_image_path' => 'video_thumbnails',
      'emthumb_custom_alt' => 1,
      'emthumb_custom_title' => 1,
      'emthumb_store_local_thumbnail' => 1,
      'emthumb_start_collapsed' => 0,
      'default_value' => array(
        '0' => array(
          'embed' => '',
          'value' => '',
          'emthumb' => array(
            'emthumb' => array(
              'emthumb' => '',
            ),
          ),
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Video',
      'weight' => '-4',
      'description' => '',
      'type' => 'emvideo_textfields',
      'module' => 'emvideo',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Video');

  return $fields;
}
