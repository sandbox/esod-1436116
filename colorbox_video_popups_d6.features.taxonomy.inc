<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function colorbox_video_popups_d6_taxonomy_default_vocabularies() {
  return array(
    'video' => array(
      'name' => 'Video',
      'description' => '',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '1',
      'multiple' => '0',
      'required' => '0',
      'tags' => '0',
      'module' => 'features_video',
      'weight' => '0',
      'nodes' => array(
        'video' => 'video',
      ),
    ),
  );
}
